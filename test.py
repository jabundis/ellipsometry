import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as cons
import main 

def test():
    n1 = 1; n2 = 2.1
    phi1 = 45*np.pi/180

    phi2 = np.arcsin(n1*np.sin(phi1)/n2)

    #S polarization
    ts = 2*n1*np.cos(phi1) / ( n1*np.cos(phi1) + n2*np.cos(phi2) )
    rs = (n1*np.cos(phi1) - n2*np.cos(phi2)) / (n1*np.cos(phi1) + n2*np.cos(phi2))

    #P polarization
    tp = 2*n1*np.cos(phi1) / ( n1*np.cos(phi2) + n2*np.cos(phi1) )
    rp = (n2*np.cos(phi1) - n1*np.cos(phi2)) / (n2*np.cos(phi1) + n1*np.cos(phi2))


    n = [n1, n2]
    L = []
    device = main.Device(n, L)
    sim = main.Ellipsometer(3e9, phi1*180/np.pi, device)

    #S polarization Ellip. Code
    TotalMat_s = sim.get_totalMat("s")
    ts_ellipCode = np.linalg.det(TotalMat_s) / TotalMat_s[1,1] 
    rs_ellipCode = -TotalMat_s[1,0] / TotalMat_s[1,1] 

    #P polarization Ellip. Code
    TotalMat_p = sim.get_totalMat("p")
    tp_ellipCode = np.linalg.det(TotalMat_p) / TotalMat_p[1,1] 
    rp_ellipCode = -TotalMat_p[1,0] / TotalMat_p[1,1] 
    
    toCompare = np.array( (rs,ts,rp,tp) )  
    fromCode = np.array( (rs_ellipCode,ts_ellipCode,rp_ellipCode,tp_ellipCode) )
    if( np.all(fromCode - toCompare <= 1e-15) ):
        print("...... Reflection and Transmission Coef. test passed")
    else: 
        print("Reflection and Transmission Coef. test failed")
    return ( toCompare, fromCode )


def test2():
    n1 = 1; n2 = 2.1
    phi1 = 45*np.pi/180

    phi2 = np.arcsin(n1*np.sin(phi1)/n2)

    #S polarization
    ts = 2*n1*np.cos(phi1) / ( n1*np.cos(phi1) + n2*np.cos(phi2) )
    rs = (n1*np.cos(phi1) - n2*np.cos(phi2)) / (n1*np.cos(phi1) + n2*np.cos(phi2))

    #P polarization
    tp = 2*n1*np.cos(phi1) / ( n1*np.cos(phi2) + n2*np.cos(phi1) )
    rp = (n2*np.cos(phi1) - n1*np.cos(phi2)) / (n2*np.cos(phi1) + n1*np.cos(phi2))

    n = [n1, n2]
    L = []
    device = main.Device(n, L)
    sim = main.Ellipsometer(3e9, phi1*180/np.pi, device)
    (rs_Code, ts_Code) = sim.get_refAndtrans('s')
    (rp_Code, tp_Code) = sim.get_refAndtrans('p')

    toCompare = np.array( (rs,ts,rp,tp) )  
    fromCode = np.array( (rs_Code,ts_Code,rp_Code,tp_Code) )
    if( np.all(fromCode - toCompare <= 1e-15) ):
        print("...... Reflection and Transmission Coef. test passed")
    else: 
        print("Reflection and Transmission Coef. test failed")
    return ( toCompare, fromCode )

def test3():
    n_0 = 1
    nSubst = 3.8714 + 1j*0.016
#    nSubst = 3.8714
    nAlOx = 1.77

    wavelen = 632e-9
    phi_0=65

    w = 2*np.pi*cons.c/wavelen
    d = 2e-9

    #experimental
    Psi = 25.74; Delta = 171.28

    device = main.Device([n_0, nAlOx, nSubst], [d])
    sim = main.Ellipsometer(w, phi_0, device)
    return sim.get_DeltaAndPsi()

def test4():
    n_0 = 1
    nSubst = 3.8714 + 1j*0.016
#    nSubst = 3.8714
    nAlOx = 1.77

    wavelen = 632e-9
    phi_0=70

    w = 2*np.pi*cons.c/wavelen
    lengths = np.linspace(0, 350e-9, 1001)
    Delta = np.zeros(1001)
    Psi = np.zeros(1001)

    #experimental
    Psi_exp = 25.74; Delta_exp = 171.28

    device = main.Device([n_0, nAlOx, nSubst], [lengths[0]])
    sim = main.Ellipsometer(w, phi_0, device)
    Delta[0], Psi[0] = sim.get_DeltaAndPsi()
    for i in range(len(lengths)-1):
        d = lengths[i+1]
        sim.device.modify_len(d, 1)
        Delta[i+1], Psi[i+1] = sim.get_DeltaAndPsi()

    fig, [ax1, ax2] = plt.subplots(2,1)
    ax1.plot(lengths, Delta)
    ax1.set_xlabel('d')
    ax1.set_ylabel('Delta')
    ax2.plot(lengths, Psi) 
    ax2.set_xlabel('d')
    ax2.set_ylabel('Psi')
    plt.legend()

    plt.figure()
    plt.plot(Delta, Psi)
    plt.xlabel('Delta')
    plt.ylabel('Psi')

#    return (lengths, Delta, Psi)
    
def test5():
    n_0 = 1
    nSubst = 3.8714 + 1j*0.016
    nAlOx = 1.77

    wavelen = 632e-9
    phi_0=70

    lengths = np.linspace(0, 350e-9, 1001)

    n = [n_0, nAlOx, nSubst]
    L = [0]
    
    sim = main.Simulation(n, L, wavelen, phi_0, {1:lengths}, Ellipsometer=main.Ellipsometer_2)
    return sim.getPlot()

def test6():
    #Data
    n_0 = 1
    nSubst = 3.8714 + 1j*0.016
    nAlOx = 1.77
    nSam = 1.4
    
    d_AlOx = 2e-9

    d_Sam_min = 0
    d_Sam_max = 350e-9
    num = 1001

    wavelen = 632e-9
    phi_0=70

    Delta_experiment = 166.8139 
    Psi_experiment = 10.1978

    #Calculations
    lengths = np.linspace(d_Sam_min, d_Sam_max, num)

    n = [n_0, nSam, nAlOx, nSubst]
    L = [0, d_AlOx] #The 0 is ignored. Length of Sam is changed for "lenghts" 
                    #inside the "Simulation" code

    experimentalData = [Delta_experiment, Psi_experiment]
    
    sim = main.Simulation(n, L, wavelen, phi_0, {1:lengths}, experimentalData, main.Ellipsometer)
    return sim.getPlot()
