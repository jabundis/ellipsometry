import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import c

def cosPhi(n, phi_0, n_0=1):
    """
    Returns Cosine of angle with respect to normal at medium with refractive 
    index "n" given an initial angle of incidence phi_0 in an incidence medium 
    with Ref. Index n_0

    phi_0 [float] -> incidence angle in degrees
    """
    return np.sqrt( 1 - (n_0 * np.sin( (phi_0/180)*np.pi ) / n)**2 )


def cosPhi_t(n, phi_0, n_0=1):
    """
    Returns Cosine of angle with respect to normal at medium with refractive 
    index "n" given an initial angle of incidence phi_0 in an incidence medium 
    with Ref. Index n_0

    phi_0 [float] -> incidence angle in degrees
    """
    cos_1 = np.sqrt( 1 - (n_0 * np.sin( (phi_0/180)*np.pi ) / n)**2 ) 

    #Choose square root that provides absorption
    #(Assuming exp(ikx), and ref. index n=a+ib with a>0, b>=0)
    k = n * cos_1
    Sign = np.sign(np.imag(k))
    if(Sign==0): Sign=1
    return cos_1 * Sign
    

def transCoef(n1, n2, phi_0, n_0=1, polarization='s'):
    """
    Returns transmission coefficient for light going from medium n1 to n2
    """
    if(polarization=='s'):
        t = 2*n1*cosPhi(n1, phi_0, n_0) / ( n1*cosPhi(n1, phi_0, n_0) + 
                                            n2*cosPhi(n2, phi_0, n_0) )
    elif(polarization=='p') :
        t = 2*n1*cosPhi(n1, phi_0, n_0) / ( n1*cosPhi(n2, phi_0, n_0) + 
                                            n2*cosPhi(n1, phi_0, n_0) )
    return t


def refCoef(n1, n2, phi_0, n_0=1, polarization='s'):
    """
    Returns reflection coefficient for light going from medium n1 to n2
    """
    if(polarization=='s'):
        r = ( n1*cosPhi(n1, phi_0, n_0) - n2*cosPhi(n2, phi_0, n_0) )  / ( 
                             n1*cosPhi(n1, phi_0, n_0) + n2*cosPhi(n2, phi_0, n_0) )
    elif(polarization=='p') :
        r = ( n2*cosPhi(n1, phi_0, n_0) - n1*cosPhi(n2, phi_0, n_0) )  / ( 
                             n1*cosPhi(n2, phi_0, n_0) + n2*cosPhi(n1, phi_0, n_0) )
    return r


def transmission(n1, n2, phi_0, n_0=1, polarization='s'):
    """
    2 by 2 complex ndarray with transmission going from medium n1 to n2 for a 
    given polarization.
    """
    t21 = transCoef(n2, n1, phi_0, n_0, polarization)

    r12 = refCoef(n1, n2, phi_0, n_0, polarization)

    T = np.zeros((2,2), dtype=complex)
    T[0,0] = 1;    T[0,1] = -r12
    T[1,0] = -r12; T[1,1] = 1
    T = (1/t21) * T
    return T

def propagate(n, d, w, phi_0, n_0=1):
    """
    2 by 2 complex ndarray with propagation across media n 
    """
    P = np.zeros((2,2), dtype=complex)
    P[0,0] = np.exp(1j*w*n*d*cosPhi(n, phi_0, n_0) / c)    
    P[1,1] = np.exp(-1j*w*n*d*cosPhi(n, phi_0, n_0) / c)

    return P

def closestP(Delta_exp, Psi_exp, Delta, Psi):
    """
    returns Index of elements in Delta and Psi closest to the experimental data
    and its distance.
    """
    minimum = (Delta_exp-Delta[0])**2 + (Psi_exp-Psi[0])**2
    ind_m = 0
    for i in range(len(Delta) - 1):
        ind = i + 1 
        d_sq = (Delta_exp-Delta[ind])**2 + (Psi_exp-Psi[ind])**2
        if(d_sq<minimum):
            minimum = d_sq
            ind_m = ind
    return (ind_m, np.sqrt(minimum))

#class Device(object):
#    def __init__(self, materials, thicknesses):
#        pass

class Device(object):
    def __init__(self, n, L):
        """
        n [array-like] -> Ref. indices of materials in the order: incidence
                          medium, layers, and substrate. 
                          (respecting the order of neighbors)
        L [array-like] -> thicknesses of the layers
        """
        self.n = n
        self.L = np.concatenate(([np.NaN], L, [np.NaN]))

        if(len(self.L)!=len(n)): 
            raise Exception('Incorrect length of array "n" or "L"')

    def modify_len(self, length, m):
        """
        Modify the length of the "m-th" medium (counting from zero).
        Length of incidence and substrate cannot be modified (remain infinite).
        """
        if(m!=0 and m!=len(self.n)-1):
            self.L[m] = length 

    def get_refInd(self, m):
        """
        Retrieves ref. index of 'm-th' medium (counting from 0) starting 
        from incidence material and ending with the substrate. 

        m is an integer >= 0 
        """
        return self.n[m]

    def get_length(self, m):
        """
        Retrieves length of 'm-th' medium. Incidence and Subtrate media are infinite
        and return np.NaN

        m is an integer >= 0
        """
        return self.L[m]

    def get_nLayers(self):
        return len(self.L) - 2


class Ellipsometer(object):
    def __init__(self, w, phi_0, device):
        """
        w [float] -> angular freq. of excitation
        phi_0 [float] -> angle of incidence in degrees
        """
        self.w = w
        self.phi_0 = phi_0
        self.device = device

    def set_device(self, device):
        self.device = device

    def get_transMat(self, m, polarization):
        """
        Returns transmission matrix for light going from medium "m" to "m+1".
        Note: incidence medium is number "0".

        m [int] -> interface (m >= 0)
        polarization [string] -> 's' or 'p' (case-insensitive)

        Output: complex 2d ndarray
        """
        polarization = polarization.lower()
        n_0 = self.device.get_refInd(0) #Incidence ref. Ind.

        n1 = self.device.get_refInd(m)
        n2 = self.device.get_refInd(m+1)

        return transmission(n1, n2, self.phi_0, n_0, polarization)


    def get_propagationMat(self, m):
        """
        Returns propagation matrix across medium "m". 
        (Returns an Exception for incidence and substrate)

        m [int] -> number of medium 

        Output: complex 2d ndarray
        """
        if( m==0 or m==len(self.device.n)-1 ): 
            return Exception("incident and substrate media are infinite")
        n_0 = self.device.get_refInd(0) #Incidence ref. Ind.
        n = self.device.get_refInd(m)
        d = self.device.get_length(m)

        return propagate(n, d, self.w, self.phi_0, n_0)
    

    def get_totalMat(self, polarization):
        """
        Returns total transfer matrix.

        polarization [string] -> 's' or 'p' (case-insensitive)

        Output: complex 2d ndarray
        """
        T = self.get_transMat(0, polarization) #Placeholder for full transformation
        for i in range(self.device.get_nLayers()):
            m = i + 1 #medium number
            prop = self.get_propagationMat(m)
            trans = self.get_transMat(m, polarization) 
            layerTrans = np.matmul(trans, prop) #transformation for crossing medium 'm'
            T = np.matmul(layerTrans, T)
        return T

    def get_refAndtrans(self, polarization):
        """
        Returns reflection and transmission coefficients for a given polarization.

        polarization [string] -> 's' or 'p' (case-insensitive)

        Output: 
            (r, t) [tuple] -> r and t are the reflection and transmission Coef., respectively.
                              These are complex numbers.

        """
        A = self.get_totalMat(polarization)
        r = -A[1,0] / A[1,1]
        t = np.linalg.det(A) / A[1,1]

        return (r, t)

    def get_DeltaAndPsi(self):
        (rs, ts) = self.get_refAndtrans('s')
        (rp, tp) = self.get_refAndtrans('p')
       
        def getPhase(z):
            phase = np.angle(z, deg=True)
            if(phase<0): phase += 360
            return phase

        Delta = getPhase(rp) - getPhase(rs)
        if(Delta<0): Delta += 360

        Psi = np.arctan2( np.abs(rp) , np.abs(rs) ) * 180/np.pi
        if(Psi<0): Psi += 360
        return (Delta, Psi)

class Ellipsometer_2(Ellipsometer):
    def get_DeltaAndPsi(self):
        (rs, ts) = self.get_refAndtrans('s')
        (rp, tp) = self.get_refAndtrans('p')
       
        Delta = np.angle(rp, deg=True) - np.angle(rs, deg=True) 

        Delta = abs(Delta)
        if(Delta>180): Delta = 360 - Delta

        Psi = np.arctan2( np.abs(rp) , np.abs(rs) ) * 180/np.pi
        if(Psi<0): Psi += 360
        return (Delta, Psi)

class Simulation(object):
    """
    Simulates different thicknesses for a desired layer
    """
    def __init__(self, n, L, lambda_0, phi_0, newLengths, 
                  experimentalData=[None, None], Ellipsometer=Ellipsometer):
        """
        n [array-like] -> Ref. indices of materials in the order: incidence
                          medium, layers, and substrate. 
                          (respecting the order of neighbors)
        L [array-like] -> initial thicknesses of the layers
        lambda_0 [float] -> excitation wavelength
        phi_0 [float] -> angle of incidence in degrees
        newLengths [dictionary] -> Index corresponds to medium number starting
                                   from zero (incidence medium takes the value 0).
                                   Values are array-like with the thicknesses to
                                   simulate for the corresponding medium.
        experimentalData [array-like] -> array with experimental Delta and Psi
                                         i.e., [Delta_exp, Psi_exp]
        """
        self.n = n 
        self.L = L
        self.lambda_0 = lambda_0
        self.phi_0 = phi_0 
        self.newLengths = newLengths
        
        self.experimentalData  = experimentalData
        self.Ellipsometer = Ellipsometer

    def run(self):
        w = 2*np.pi*c/self.lambda_0

        device = Device(self.n, self.L)
        ellip = self.Ellipsometer(w, self.phi_0, device)
        
        #Get medium and lenghts
        for medium in self.newLengths:
            lengths = self.newLengths[medium]

        size = len(lengths)

        Delta = np.zeros(size)
        Psi = np.zeros(size)
        
        for i, d in enumerate(lengths):
            ellip.device.modify_len(d, medium)
            Delta[i], Psi[i] = ellip.get_DeltaAndPsi()
        
        #Calculate layer thickness
        Delta_exp, Psi_exp = self.experimentalData
        if(isinstance(Delta_exp, (int, float)) and isinstance(Psi_exp, (int, float))):
            #Evaluate closest point between simulation and measurement
            ind, minDistance = closestP(Delta_exp, Psi_exp, Delta, Psi)
            closestPoint = (Delta[ind], Psi[ind])
            predictedThickness = lengths[ind]
            prediction = (predictedThickness, closestPoint)

        else: 
            prediction = None
        
        return (lengths, Delta, Psi, prediction)


    def getPlot(self):
        (lengths, Delta, Psi, prediction) = self.run()
        
        if(prediction!=None): 
            plotExp = True
            predictedThickness, closestPoint = prediction
            Delta_exp, Psi_exp  = self.experimentalData
        else: plotExp = False

        fig, [ax1, ax2] = plt.subplots(2,1)

        ax1.plot(lengths, Delta)
        if(plotExp):
            ax1.plot([lengths[0], lengths[-1]],[Delta_exp, Delta_exp], 'g--')
            ax1.plot([predictedThickness] , [closestPoint[0]] , 'ro')
        ax1.set_xlabel('d')
        ax1.set_ylabel('Delta')

        ax2.plot(lengths, Psi) 
        if(plotExp):
            ax2.plot([lengths[0], lengths[-1]],[Psi_exp, Psi_exp], 'g--')
            ax2.plot([predictedThickness] , [closestPoint[1]] , 'ro')
        ax2.set_xlabel('d')
        ax2.set_ylabel('Psi')

        plt.figure()
        plt.plot(Delta, Psi)
        if(plotExp):
            plt.plot([Delta_exp, Delta_exp] , [min(Psi), max(Psi)] , 'g--')
            plt.plot([min(Delta), max(Delta)] , [Psi_exp, Psi_exp] , 'g--')
            plt.plot([closestPoint[0]] , [closestPoint[1]] , 'ro')
        plt.xlabel('Delta')
        plt.ylabel('Psi')

        return predictedThickness

